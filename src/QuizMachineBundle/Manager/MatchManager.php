<?php

namespace QuizMachineBundle\Manager;

use Doctrine\ORM\EntityManager;
use QuizMachineBundle\Entity\Category;
use QuizMachineBundle\Entity\Match;
use QuizMachineBundle\Entity\Player;
use Symfony\Component\HttpFoundation\Session\Session;

class MatchManager
{
    private $session;
    private $em;
    private $match;
    private $quizParameters;
    private $category;

    /**
     * @return mixed
     */
    public function getMatch()
    {
        return $this->match;
    }

    public function __construct(Session $session, EntityManager $em, $quizParameters)
    {
        $this->session = $session;
        $this->em = $em;
        $this->quizParameters = $quizParameters;
    }

    public function setPlayers($name1, $email1, $name2, $email2)
    {
        $now = new \DateTime();
        $player1 = new Player();
        $player1->setName($name1);
        $player1->setEmail($email1);
        $player1->setWhen($now);
        $this->em->persist($player1);
        $player2 = new Player();
        $player2->setName($name2);
        $player2->setEmail($email2);
        $player2->setWhen($now);
        $this->em->persist($player2);
        $this->em->flush();
        $this->match = new Match();
        $this->match->setPlayer1($player1);
        $this->match->setPlayer2($player2);
    }

    public function setCategory(Category $category)
    {
        $this->category = $category;
    }

    public function init()
    {
        $category = $this->em->getRepository('QuizMachineBundle:Category')->findOneByRunning(true);
        $this->category = $category;
        $this->match->setHowManyQuestions($this->quizParameters['questions']);
        $this->match->setQuestionTimeout($this->quizParameters['timeout']);
        $this->populateQuestions();
        $this->match->setPointsPlayer1Sum(0);
        $this->match->setPointsPlayer2Sum(0);
    }

    public function loadMatch()
    {
        $this->session->start();
        $this->match = $this->session->get('match');
        $this->category = $this->match->getCategory();
        $this->quizParameters['questions'] = $this->match->getHowManyQuestions();
        $this->quizParameters['timeout'] = $this->match->getQuestionTimeout();
    }

    public function saveMatch()
    {
        $this->session->start();
        $this->session->set('match', $this->match);
    }

    private function populateQuestions()
    {
        $questions = $this->em->getRepository('QuizMachineBundle:Question')->findByCategory($this->category);

        if (sizeof($questions) < $this->quizParameters['questions']) {
            throw new \Exception('Troppe poche domande nella categoria '.$this->category->getTitle());
        }
        $list = [];
        foreach ($questions as $question) {
            $list[] = $question;
        }
        $questionsToDo = [];
        while (sizeof($questionsToDo) < $this->quizParameters['questions']) {
            $rand = rand(1, sizeof($questions)) - 1;
            if (isset($list[$rand])) {
                $questionsToDo[] = $list[$rand];
                unset($list[$rand]);
            }
        }
        $this->match->setQuestionsToDo($questionsToDo);
    }

    public function nextQuestion()
    {
        $stack = $this->match->getQuestionsToDo();
        $next = array_pop($stack);
        $this->match->setQuestionsToDo($stack);
        $this->match->setCurrentQuestion($next);
    }

    public function checkAnswers($p1, $p2, $t1 = 0, $t2 = 0)
    {
        $a1 = $this->match->getCurrentQuestion()->isCorrect($p1);
        $a2 = $this->match->getCurrentQuestion()->isCorrect($p2);
        $points1 = (int)($a1 * (($this->quizParameters['timeout'] - $t1) / $this->quizParameters['timeout']) * 10);
        $points2 = (int)($a2 * (($this->quizParameters['timeout'] - $t2) / $this->quizParameters['timeout']) * 10);
        $this->match->setPointsPlayer1Sum($this->match->getPointsPlayer1Sum()+$points1);
        $this->match->setPointsPlayer2Sum($this->match->getPointsPlayer2Sum()+$points2);
        return [
            'a1' => $a1,
            'a2' => $a2,
            'points1' => $points1,
            'points2' => $points2
        ];
    }

    public function doneCurrentQuestion(){
        $stack = $this->match->getQuestionsDone();
        if ($stack==null) {$stack=[];}
        array_push($stack,$this->match->getCurrentQuestion());
        $this->match->setQuestionsDone($stack);
        $this->match->setCurrentQuestion(null);
    }
}
