#!/usr/bin/env bash


echo "Update packages"
cp -r /vagrant/provision/linuxConfiguration/etc/apt/* /etc/apt
apt-get update -q
apt-get upgrade -y

echo "Installing packages"
apt install sudo wget curl git-core vim elinks apache2  php-common \
        libapache2-mod-php  php-cli php-curl php-intl php-zip  \
        php-gd php-mcrypt php-mysql php-xml php-json php-mbstring php-soap \
        php-bcmath  php-dev -y

if [ ! -f  /usr/local/bin/composer ]; then
    echo "Get composer"
    curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin
    mv /usr/bin/composer.phar /usr/bin/composer
    chmod a+x /usr/bin/composer
fi
if ! [ -L /var/www ]; then
    echo "Check apache2"
    cp -r /vagrant/provision/linuxConfiguration/etc/apache2/* /etc/apache2
    a2enmod rewrite
    service apache2 restart
fi
if [ ! -f /usr/sbin/mysqld ]; then
    echo "Installing packages - mysql"
    DEBIAN_FRONTEND=noninteractive apt-get  -y install mysql-server
    mysqladmin -u root password root
fi
if ls -c1 /var/lib/mysql | grep quiz; then
     echo "Database exists nothing to do"
else
     echo "Create database"
     mysqladmin -u root -proot create quiz
fi
if [ ! -f /vagrant/vendor ]; then
    echo "Get vendors via composer"
    cd /vagrant
    composer install 
fi
if [ ! -f /usr/lib/php/20151012/xdebug.so ]; then
  cd /tmp
  wget http://xdebug.org/files/xdebug-2.5.4.tgz
  tar -xvzf xdebug-2.5.4.tgz
   cd xdebug-2.5.4
   phpize
   ./configure
  make
  cp modules/xdebug.so /usr/lib/php/20151012



  cat <<EOF >> /etc/php/7.0/apache2/php.ini
  zend_extension = /usr/lib/php/20151012/xdebug.so
[Xdebug]

#xdebug.remote_connect_back = 1
xdebug.remote_enable = 1
xdebug.remote_handler = "dbgp"
xdebug.remote_port = 9000
xdebug.idekey = "PHPSTORM"
xdebug.remote_host=33.33.33.1
xdebug.autostart=1
EOF
fi

