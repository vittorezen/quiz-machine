<?php

namespace QuizMachineBundle\Controller;

use QuizMachineBundle\Entity\Match;
use QuizMachineBundle\Entity\Player;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;


class DefaultController extends Controller
{
    /**
     * @Route("/", name="insert_players")
     * @Template()
     */
    public function insertPlayerAction()
    {
        $em = $this->getDoctrine()->getManager();
        $category = $em->getRepository('QuizMachineBundle:Category')->findOneByRunning(true);
        return array('category' => $category);
    }

    /**
     * @Route("/start", name="setup_game")
     * @Template()
     */
    public function startAction(Request $request)
    {
        $matchManager = $this->get('match.manager');
        $matchManager->setPlayers(
            $request->get('name_1'),
            $request->get('email_1'),
            $request->get('name_2'),
            $request->get('email_2')
        );
        $matchManager->init();
        $matchManager->saveMatch();
        return array('match'=>$matchManager->getMatch());
    }
    /**
     * @Route("/question", name="get_question")
     * @Template()
     */
    public function questionAction(Request $request)
    {
        $matchManager = $this->get('match.manager');
        $matchManager->loadMatch();
        if ($matchManager->getMatch()->matchIsFinished()) {
            return $this->redirect($this->generateUrl('show_result', array()));
        }
        if (
            (sizeof($matchManager->getMatch()->getQuestionsToDo())==0) &&
            (sizeof($matchManager->getMatch()->getQuestionsDone())==0)
        ){
            $matchManager->init();
            $matchManager->saveMatch();
        }

        $matchManager->nextQuestion();
        $matchManager->saveMatch();
        return array('match'=>$matchManager->getMatch());
    }



    /**
     * @Route("/show/question-result", name="show_current_question_result")
     * @Template()
     */
    public function questionResultAction(Request $request)
    {
        $p1=$request->get('p1');
        $p2=$request->get('p2');
        $t1=$request->get('t1');
        $t2=$request->get('t2');
        $matchManager = $this->get('match.manager');
        $matchManager->loadMatch();
        $result=$matchManager->checkAnswers($p1,$p2,$t1,$t2);
        $matchManager->doneCurrentQuestion();
        $matchManager->saveMatch();
        return array('match'=>$matchManager->getMatch(),'result'=>$result);
    }

    /**
     * @Route("/show/result", name="show_result")
     * @Template()
     */
    public function resultAction(Request $request)
    {
        $matchManager = $this->get('match.manager');
        $matchManager->loadMatch();
        return array('match'=>$matchManager->getMatch());
    }

}
