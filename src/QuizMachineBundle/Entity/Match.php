<?php

namespace QuizMachineBundle\Entity;


class Match
{
    private $player1;
    private $player2;
    private $questions;
    private $howManyQuestions;
    private $questionTimeout;
    private $questionsDone;
    private $questionsToDo;
    private $currentQuestion;
    private $category;
    private $pointsPlayer1Sum;
    private $pointsPlayer2Sum;

    /**
     * @return mixed
     */
    public function getPointsPlayer1Sum()
    {
        return $this->pointsPlayer1Sum;
    }

    /**
     * @param mixed $pointsPlayer1Sum
     */
    public function setPointsPlayer1Sum($pointsPlayer1Sum)
    {
        $this->pointsPlayer1Sum = $pointsPlayer1Sum;
    }

    /**
     * @return mixed
     */
    public function getPointsPlayer2Sum()
    {
        return $this->pointsPlayer2Sum;
    }

    /**
     * @param mixed $pointsPlayer2Sum
     */
    public function setPointsPlayer2Sum($pointsPlayer2Sum)
    {
        $this->pointsPlayer2Sum = $pointsPlayer2Sum;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return mixed
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * @param mixed $questions
     */
    public function setQuestions($questions)
    {
        $this->questions = $questions;
    }


    /**
     * @return Player
     */
    public function getPlayer1()
    {
        return $this->player1;
    }

    /**
     * @param mixed $player1
     */
    public function setPlayer1(Player $player1)
    {
        $this->player1 = $player1;
    }

    /**
     * @return Player
     */
    public function getPlayer2()
    {
        return $this->player2;
    }

    /**
     * @param mixed $player2
     */
    public function setPlayer2(Player $player2)
    {
        $this->player2 = $player2;
    }

    /**
     * @return mixed
     */
    public function getHowManyQuestions()
    {
        return $this->howManyQuestions;
    }

    /**
     * @param mixed $howManyQuestions
     */
    public function setHowManyQuestions($howManyQuestions)
    {
        $this->howManyQuestions = $howManyQuestions;
    }

    /**
     * @return mixed
     */
    public function getQuestionTimeout()
    {
        return $this->questionTimeout;
    }

    /**
     * @param mixed $questionTimeout
     */
    public function setQuestionTimeout($questionTimeout)
    {
        $this->questionTimeout = $questionTimeout;
    }

    /**
     * @return mixed
     */
    public function getQuestionsDone()
    {
        return $this->questionsDone;
    }

    /**
     * @param mixed $questionsDone
     */
    public function setQuestionsDone($questionsDone)
    {
        $this->questionsDone = $questionsDone;
    }

    /**
     * @return mixed
     */
    public function getQuestionsToDo()
    {
        return $this->questionsToDo;
    }

    /**
     * @param mixed $questionsToDo
     */
    public function setQuestionsToDo($questionsToDo)
    {
        $this->questionsToDo = $questionsToDo;
    }

    /**
     * @return mixed
     */
    public function getCurrentQuestion()
    {
        return $this->currentQuestion;
    }

    /**
     * @param mixed $currentQuestion
     */
    public function setCurrentQuestion($currentQuestion)
    {
        $this->currentQuestion = $currentQuestion;
    }


    public function matchIsFinished()
    {
        return (sizeof($this->questionsDone) == $this->howManyQuestions);
    }




}
